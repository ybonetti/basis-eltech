# Diodenverhalten und DC-Prober

## Ziel: Grundkenntnisse zu

- Eigenschaften von Widerstand und Diode
  - Ohm'sches Gesetz, Gleichstromleistung
  - Durchlass- und Sperrspannung
- Vierpolmessungen
- Programmierung von DC-Rampen

## Literatur

- [Wikipedia-Eintrag "Diode"]( http://de.wikipedia.org/wiki/Diode )
- [Wikipedia-Eintrag "Widerstand_(Bauelement)"]( http://de.wikipedia.org/wiki/Widerstand_%28Bauelement%29 )
- `/ftt/Schulung/Dokumente/elektrotechnik.pdf` für Grundlagen zu Widerständen und Spannungsteilern 

## Vorbereitung

### Material

- Widerstand 1 kOhm (mögliche: 470 Ohm bis 2.2 kOhm)
- Leuchtdiode (LED) beliebiger Farbe

### Schaltung

- Serienschaltung von Widerstand (R1) und LED mit Anschlusspunkten
  _pw, pdw, pd,_ sodass Widerstand zwischen _pw_ und _pdw_ liegt, und
  LED zwischen _pdw_ und _pd_
- Polarität von LED ist irrelevant, soll durch Messung herausgefunden werden

#### Messgrössen

- Eingangssspannung _U0_ und Gesamtstrom _I0_ gemessen von _pw_ nach _pd_
  (dh positiv, wenn Pluspol an _pw_ liegt)
- Spannung _Uw_ über Widerstand, gemessen von _pw_ nach _pdw_
- Spannung _Ud_ über LED, gemessen von _pdw_ nach _pd_

## Experiment

### Sondenanschlüsse

1 an _pw_,  2 an _pd_, 3 an _pdw_, 4 an _pd_

dh _U0_ gemessen zwischen 1 und 2, _I0_ von 1 nach 2, _Ud_ zwischen 3 und 4

### DC-Prober-Programm

Vierpolmessung, Kennlinienaufnahme:

- Stellgrösse _I0_ etwa von -10mA bis +10mA
- Schrittgrösse etwa 0.2mA
- Begrenzung von _U0_ auf etwa +/- 10V
- Messgrösse _Ud_

### Auswertung

- Darstellung von _Ud_ und (rechnerisch) _Uw_ als Funktion von _I0_
- Schaltschema mit allen Messgrössen

## Fragen

- Wodurch ist _Uw_ bestimmt, wodurch _Id_ (Diodenstrom)?
- In welchem Bereich von _Ud_ leuchtet die LED? Was ist ihre Durchlasspannung?
- In welchem Bereich liegt die Sperrspannung?
- Was ist die Polarität der LED, dh wo liegen Anode und Kathode in der
  vorliegenden Schaltung?
- Wie gross ist die an der LED umgesetzte Leistung _Pled_ absolut,
  sowie relativ zur Gesamtleistung _P0_ ,
  jeweils als Funktion von _Ud_ und _Id_ ?

---

_(2013-7-23, Y.Bonetti)_
