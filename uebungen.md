# Uebungen

## Grundlagen

### Spannung, Strom, Energie

**(1)** Eine Batterie mit der Spannung von 12 V habe eine Kapazität von
80 Ah, dh während 80 Stunden kann sie einen Strom von 1 A liefern, oder
80 A während 1 Stunde. Wie gross ist die in der Batterie gespeicherte
Energie?

**(2)** Wenn man ein Netzgerät mit einer Ausgangsleistung von 10 W hat,
wie lange benötigt man, um damit eine 12V-Batterie mit 80 Ah Kapazität
zu laden?

**(3)** Ein Fehlerstromschutzschalter für 230 V habe einen Auslösestrom
von 20 mA. Wie gross darf der Leitwert zwischen seinen Anschlüssen sein,
damit der Schalter gerade noch nicht auslöst?

**(4)** Wie sehen die Strom-Spannungs-Kennlinien eines idealen Voltmeters
und eines idealen Amperemeters aus?

**(5)** Wie sieht die Strom-Spannungs-Kennlinie einer Glühbirne aus, die
im kalten Zustand einen Widerstand von etwa 200 Ohm hat, bei etwa 5 V zu
leuchten beginnt (dh dort ändert ihr Widerstand) und bei ihrer normalen
Arbeitsspannung von 12 V einen Strom von 0.4 A benötigt? Für diesen
idealisierten Fall soll die Kennlinie aus zwei geraden Stücken bestehen.

### Schaltungen

**(6)** Wie sieht ein Schaltbild aus, bei welchem eine Glühlampe über
einen Taster an eine Batterie angeschlossen wird und gleichzeitig sowohl
ihr Strom als auch die Spannung über der Lampe gemessen werden soll?

**(7)** Welche Verbindungen müssen an einem Umschaltrelais gelegt werden,
damit es als Summer (Selbstunterbrecher) verwendet werden kann?

**(8)** Wie sieht ein Schaltbild aus, bei welchem ein Wecker von zwei
unabhängigen Tastern aus betätigt werden kann, nachdem er mit einem
Hauptschalter eingeschaltet wurde?

**(9)** Wie müssen zwei Umschalter verbunden werden, damit eine
Glühlampe durch jeden von ihnen unabhängig ein- und ausgeschaltet
werden kann (zB bei zwei Türen eines Raumes)?

## Elektrische Quellen

**(10)** Eine frische 4.5 V Flachbatterie habe eine Leerlaufspannung
von 4.7 V. Wenn man ein Glühlämpchen anschliesst, das einen Strom
von 100 mA zieht, falle die Klemmenspannung der Batterie auf 4.4 V
zusammen. Wie gross ist damit ihr Innenwiderstand, und wie gross müsste
ihr Kurzschlussstrom sein?

**(11)** Wann darf man ein Netzgerät kurzschliessen, wann eine Batterie?

**(12)** Darf man einen Akku zum Laden direkt an ein Netzgerät anschliessen?

## Multimeter

**(13)** Ein Zeigerinstrument habe einen Innenwiderstand von 10 Ohm
und schlage bei einem Strom von 1 mA voll aus. Wie gross muss ein dazu
in Serie geschalteter Widerstand sein, wenn man mit dem Gerät bei
Vollausschlag eine Spannung von 50 V messen will?

**(14)** Ein Ampèremeter mit einem Innenwiderstand von 2 Ohm habe
einen Messbereich von 1 A. Wie gross muss ein dazu parallel geschalteter
Widerstand sein, wenn man bis zu 5 A mit dem Gerät messen möchte?

**(15)** Weshalb sollte man (zumindest bei Zeigerinstrumenten) stets
mit dem grössten Messbereich beginnen, aber dann zum kleinstmöglichen
Bereich wechseln?

**(16)** Wie prüft man mit einem Multimeter, ob eine Sicherung durchgebrannt
oder noch intakt ist?

**(17)** Wenn eine Stromversorgung nicht mehr zu funktionieren scheint,
was kann man alles mit einem Multimeter prüfen? Mindestens fünf Punkte/Dinge!

**(18)** Was ist im allgemeinen in einer Schaltung einfacher mit einem
Multimeter zu messen und weshalb: Strom oder Spannung?

## nicht-ohmsche Bauteile

### Kondensatoren

**(19)** Ein Drehkondensator bestehe aus 10 halbkreisförmigen Metallplatten,
welche auf einer drehbaren Achse nebeneinander so angeordnet sind,
dass sie zwischen 11 feststehenden, ebenfalls halbkreisförmigen
Platten ein- und ausgeschwenkt werden können. Die drehbaren Platten (Rotor)
und die feststehenden (Stator) sind jeweils miteinander leitend
verbunden. Wie gross ist die maximale Kapazität dieser Anordnung,
wenn der Luftspalt zwischen den Rotor- und Statorplatten 0.5 mm und
der Radius der Platten 2 cm beträgt?
(Man vernachlässige die Dicke der Achse.)

**(20)** Es seien zwei flache Leiterbahnen gegeben, die parallel
zueinander in einem bestimmten Abstand verlaufen.
Wann ist die Kapazität dieser Anordnung grösser: wenn sie einander
mit der flachen Seite oder mit der Kante gegenüberliegen?
(Der Luftspalt zwischen den Leitern sei in beiden Fällen derselbe.)

### Dioden

**(21)** Eine Siliziumdiode hat eine Durchlassspannung von etwa 0.7 V.
Welchen Maximalstrom verträgt sie, wenn ihre Verlustleistung höchstens 0.2 W betragen darf?
Wie gross muss ein Vorwiderstand zu dieser Diode mindestens sein,
wenn sie an eine Batterie von 12 V angeschlossen werden soll?

**(22)** Eine LED habe eine Durchlassspannung von 2 V und leuchte
gut bei einem Strom von 10 mA. Welcher Vorwiderstand sollte
zum Betrieb an 5 V aus der Serie "E6" gewählt werden?

---

_Copyright 2014,2015, Yargo Bonetti_

Dieses Werk ist unter der Creative-Commons-Lizenz vom Typ Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International lizenziert.
Um eine Kopie dieser Lizenz einzusehen, besuchen Sie <http://creativecommons.org/licenses/by-sa/4.0/> oder schreiben Sie einen Brief an Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
