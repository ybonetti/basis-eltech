# Basiswissen Elektrotechnik

Dies ist eine kurze Einführung in die wichtigsten Begriffe der Elektrotechnik,
wie sie für den erfolgreichen Umgang mit Multimetern und anderen Messgeräten
notwendig sind.

Der Text entstand als Unterrichtshilfe für eine team-interne Schulung am
[FIRST-Lab]( https://www.first.ethz.ch ), ist aber mein persönliches Werk
und hat ansonsten keinerlei Verbindung zum FIRST-Lab.

---

(2014-2016) Y.Bonetti/HB9KNS

[Lizenz:CC4-BY-SA]( license-cc4-by-sa.txt )
