# Stundenplan Basiswissen Elektrotechnik

1. Woche 1
 - Elementarladung
 - Ladungstrennung, Ladungsdifferenz
 - physikalische und technische Stromrichtung
 - Energie und Spannung
 - Strom
 - Leistung
 - Gleich- und Wechselstrom
 - Widerstand, Leitwert, Ohm'sches Gesetz

2. Woche 2
 - Strom-Spannungs-Kennlinie
 - Schaltbilder
 - Serien- und Parallelschaltungen

3. Woche 3
 - Spannungsquellen
 - Stromquellen

4. Woche 4
 - Multimeter
 - differentieller Widerstand

