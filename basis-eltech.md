# Basiswissen Elektrotechnik

Diese kurze Einführung soll einen Einblick in die Grundlagen der
Elektrotechnik geben, die zum Verständnis von Gleichstromschaltungen
sowie zur Benutzung von Multimetern notwendig sind.

Zunächst werden die Grundlagen von Ladung, Strom, Spannung, Ohm'schem
Gesetz und Leistung erklärt. Dabei verzichten wir auf Herleitungen oder
detaillierte physikalische Berechungen, beschreiben dafür aber, wie man
die entsprechende Grösse mittels üblicher Multimeter erfassen kann.
Es folgt eine Beschreibung der wichtigsten Quellen für Gleichstrom und
Gleichspannung.
Weiter werden Kondensator, Elektromagnet und Diode vorgestellt
sowie die Möglichkeiten, sie mittels Multimetern oder Waferprobern
messen oder nachweisen zu können.

## Grundlagen

### elektrische Ladung, Spannung, Strom

Elektrische Ladung ist eine physikalische Grundeigenschaft von
Elementarteilchen, genauso wie ihre Masse, und wird in der Einheit
Coulomb (C) gemessen. Teilchen können elektrisch
neutral, positiv oder negativ geladen sein. Freie elektrische
Ladungen sind quantisiert, dh es gibt einen kleinstmöglichen Wert,
die Elementarladung e<sub>0</sub>; sie beträgt etwa `1.6*10`<sup>-19</sup> C.

Wenn die Ladung in einem System nicht ausgeglichen ist, dh nicht alle
Teile neutral sind, besteht (elektrische) Spannung. Wenn infolge dieser
Spannung geladene Teilchen bewegt werden, tritt (elektrischer) Strom
auf. Zur Erzeugung von elektrischem Strom müssen deshalb elektrische
Ladungen getrennt werden, damit eine elektrische Spannung entstehen kann,
welche anschliessend wiederum Ladungen bewegen kann.

Das Elementarteilchen Elektron trägt eine negative Elementarladung -e<sub>0</sub>.
Elektronen sind die Träger der elektrischen Ladung im engeren Sinne,
die elektrotechnisch ausgenutzt wird. Ursprünglich nahm man an,
der elektrische Strom in metallischen Leitern werde durch positiv
geladene Träger verursacht, weshalb der technische Strom entgegen dem
physikalischen (Elektronen-)Strom fliesst. Für alle elektrotechnischen
Vorgänge, die keine chemischen Prozesse (zB in Batterien) umfassen,
kann man jedoch problemlos annehmen, es gebe positive Elementarladungen.
Diese fliessen, wenn eine leitende Verbindung besteht, von einem Punkt
mit höherer Spannung (positiver Pol) zu einem Punkt mit niedrigerer
Spannung (negativer Pol), was zum Ladungsausgleich führt, wenn der
Ladungsunterschied nicht aufrecht erhalten wird.

Elektrische Ladungen können chemisch getrennt werden, zB in
einer Batterie, oder physikalisch, zB mittels eines Generators. Hierbei
wird chemische, mechanische, optische (Photozellen!) oder sonstige Energie
in elektrische Energie umgewandelt.
Spannungs- und Stromquellen werden weiter unten besprochen.

#### elektrische Energie und Spannung

Zum Trennen von elektrischen Ladungen ist Energie notwendig. Dabei
wird zB mechanische Energie in elektrische Energie umgewandelt, die
anschliessend im elektrischen Feld zwischen den Ladungen gespeichert ist.

Wenn man von einem neutralen Objekt eine Ladung `-Q` entfernt, sodass
auf dem Objekt die Ladung `+Q` zurückbleibt, so muss man hierfür eine
bestimmte Energie `E` aufwenden und es entsteht zwischen den beiden
Ladungen eine Spannung `U`, die gegeben ist durch

    E=Q*U

Umgekehrt kann eine Ladung `Q` die Energie `E=Q*U` abgeben, wenn sie
sich über eine Strecke bewegt, zwischen deren Endpunkten die Spannung `U`
liegt.

Die Einheit der Energie ist Joule (J), die der Spannung ist Volt (V).

#### elektrischer Strom

Wenn wir einen Flüssigkeitsstrom betrachten, der in einer Leitung fliesst,
so beziehen wir diesen Strom auf den Querschnitt der Leitung. Strom ist
somit immer bezogen auf eine _Fläche,_ die vom Medium oder den betrachteten
Objekten durchflossen wird.

Wenn sich nun in einer bestimmten Zeit `t` eine Ladung `Q` durch eine
Fläche `F` hindurchbewegt, so ist hierdurch der Strom

    I=Q/t

gegeben. Die Einheit des Stromes ist Ampère (A).

#### Leistung

Wenn elektrischer Strom zwischen zwei Punkten fliesst, so wird Arbeit
verrichtet, und zwar mit einer Leistung `P`, die dem Produkt des Stromes `I`
mit der Spannung `U` zwischen den zwei Punkten entspricht:

    P=U*I

Sie wird in Watt (W) gemessen. Dies lässt sich aus den weiter oben genannten
Beziehungen herleiten: die Energie ist gegeben zu `E=U*Q`, und aus `I=Q/t`
ergibt sich `Q=I*t` und damit `E=U*I*t`. Nun ist jedoch Leistung definiert
als Arbeit je Zeiteinheit, dh `P=E/t=U*I`.

Im Falle von elektrischem Strom trägt jede Elementarladung
eine Arbeit e<sub>0</sub>`*U` bei, und die Gesamtleistung ergibt sich durch
die Anzahl der Elektronen pro Zeiteinheit, die das Spannungsgefälle
`U` durchqueren. Die Arbeit wird in Joule (J) gemessen oder auch in
Watt\*Sekunde, da Arbeit umgekehrt als Leistung mal Zeit dargestellt
werden kann. Somit beträgt die während der Zeit `t` durch die Leistung `P`
verrichtete Arbeit (oder Energie) `E=P*t`.

Diese Überlegungen gelten nur für zeitlich konstante Leistung;
ansonsten wird Integralrechnung benötigt, um die zeitliche Veränderung
der beteiligten Grössen erfassen zu können.

### Wechselspannungen und -ströme

Gleichstrom oder Gleichspannung liegt vor, wenn sich die entsprechende
Grösse nicht mit der Zeit ändert. Andernfalls spricht man von
Wechselspannung oder Wechselstrom im weiteren Sinne.

Im engeren Sinn bezeichnet Wechselspannung oder -strom eine Grösse
`G` (dh Strom oder Spannung), die eine Zeitabhängigkeit der Form
`G(t)=G0*sin(2*pi*f*t)` aufweist, wobei mit `G0` die sogenannte Amplitude
und mit `f` die Frequenz (Einheit Hertz, Hz) bezeichnet wird  und `pi=3.14...` .

Da sich hierbei Spannungen und Ströme fortwährend ändern, treten
zusätzliche Effekte auf, die durch Kapazitäten (Ladungsspeicher)
und Induktivitäten (Stromspeicher) bedingt werden. Im weiteren
betrachten wir nur noch Gleichspannungen oder Gleichströme sowie Ein-
und Ausschaltvorgänge.

Falls jedoch keine Induktivitäten und Kapazitäten, sondern nur
sogennante Ohm'sche Widerstände in einer Schaltung vorhanden sind,
gelten für Wechselströme dieselben Beziehungen wie für Gleichströme.
Man kann dann also im Falle von Wechselstrom so rechnen, als läge
Gleichstrom vor.

### Widerstand, Ohm'sches Gesetz, Leitwert

Die Spannung `U` und der Strom `I` sind miteinander durch den sogenannten
Widerstand `R` verknüpft, der definiert wird durch `U=R*I`. Der
Widerstand selbst kann eine Funktion von `I` oder `U` oder beliebigen
anderen Grössen im System sein; er ist nur in speziellen Fällen durch
einen konstanten Wert gegeben. Die Einheit des Widerstandes ist Ohm oder
Volt/Ampère.

Man kann statt mit dem Widerstand auch mit seinem Kehrwert arbeiten, dem
sogenannten Leitwert `S=1/R`; damit ergibt sich die Definition `I=S*U`;
die Einheit des Leitwertes ist Sievert (S) oder Ampère/Volt.

`U=R*I` wird als "Ohm'sches Gesetz" bezeichnet; richtiger wäre jedoch
"Ohm'sche Definition", da es keine physikalische Eigenschaft erklärt,
sondern eine Definition des Widerstandes darstellt und deshalb korrekt
sein _muss._

Widerstände im engeren Sinne sind Bauteile, deren elektrischer Widerstand
unter möglichst weit gefassten Bedingungen dank spezieller Konstruktion
konstant bleibt.

### Strom-Spannungs-Kennlinie

Zur graphischen Darstellung von Widerstandsmessungen wird oft die
sogenannte Strom-Spannungs-Kennlinie verwendet. Hierbei wird auf einer
Achse die Spannung, auf der anderen der zugehörige Strom dargestellt.
Zu einem konstanten Widerstand gehört in dieser Darstellung eine Gerade,
deren Steigung durch den Widerstands- oder Leitwert gegeben ist.

In der folgenden Abbildung stellt die durchgezogene Linie einen Widerstand
von 820 Ohm dar: Auf der Abszisse ist die Spannung am Widerstand in
Schritten von 500mV dargestellt, auf der Ordinate der resultierende Strom
in Schritten von 1mA. Die Steigung der überlagerten punktierten Linie
entspricht einem Leitwert von 1.22 mS = 1.22 mA/V; der Kehrwert davon
beträgt 820 Ohm.
Der Koordinatenursprung liegt hier in der Mitte des Bildschirmgitters.

![Widerstandskennlinie]( img/widerstand820ohm.jpg )

Wenn die Kennlinie verschoben oder gekrümmt ist, spricht man von einem
nichtlinearen Widerstand; ein Beispiel hierzu findet sich weiter unten
bei der Besprechung von Dioden.

Eine Kennlinie kann entweder von Hand mittels einer Quelle und einem
Multimeter oder (halb)automatisch mit einem Prober aufgenommen werden;
siehe hierzu die Beschreibung weiter unten.

#### Differentieller Widerstand

Der elektrische Widerstand, wie bisher definiert über die Beziehung `U=R*I` ,
ist oft von geringer physikalischer Bedeutung, da er nichts darüber aussagt,
wie sich eine *Änderung* von Grössen auswirkt. Hierzu wird der sogenannte
*differentielle* Widerstand `r` benötigt, welcher durch die Beziehung

    dU = r * dI

definiert ist.
Er gibt an, um welchen Wert `dU` sich die Spannung ändert,
wenn sich der Strom um `dI` ändert.

Dieser Wert kann selbstverständlich verschieden sein für verschiedene Spannungen
oder Ströme. In der graphischen Darstellung einer Strom-Spannungs-Kennlinie
ist der differentielle Widerstand als Steigung der Kennlinie in jedem ihrer
Punkte gegeben.

Der differentielle Widerstand eines ohmschen Widerstandes
ist selbstverständlich konstant und gleich seinem Wert.

### Schaltbilder (elektrische Schemata)

Zur Darstellung elektrischer Schaltungen verwendet man sogenannte
Schaltbilder, worin Leitungen als Linien und leitende Verbindungen als
Punkte dargestellt werden. Spannungen werden als Pfeile zwischen den
zugehörigen Punkten im Schaltbild, Ströme als Pfeile auf den
entsprechenden Leitungen gezeichnet.

Bauteile werden mit Symbolen wie im folgenden Bild dargestellt:

![Schaltbildsymbole]( img/schaltbildsymbole.jpg )

### Bauteilereihen (E6, E12, E24)

Widerstände und andere elektrische Bauteile mit wählbaren Eigenschaften werden
meistens in bestimmten Wertreihen hergestellt. Diese sind ungefähr
in logarithmischer Folge angeordnet, sodass das Verhältnis von
benachbarten Werten in etwa gleich bleibt.
Die verbreiteten E-Serien sind nach der n-ten Wurzel aus 10 bezeichnet,
die diesem Verhältnis in etwa entspricht.

Bei der Serie "E6" ist dies somit die 6-te Wurzel aus 10 oder etwa 1.5.
Der erste Wert jeder Serie ist 1, der zweite Wert der Serie "E6" ist also 1.5,
der dritte `1.5*1.5` oder gerundet 2.2, der vierte `1.5*2.2` oder 3.3,
der fünfte 4.7, und der sechste 6.8. `1.5*6.8` ist gerundet 10, dh dann
beginnt die nächste Dekade, mit denselben Faktoren.

Die Serie "E12" hat zusätzlich zwischen je zwei Werten von "E6" deren
geometrisches Mittel (oder etwa 1.2 mal den unteren Wert),
die Serie "E24" analog zwischen den "E12"-Werten.

### Zusammenschaltung von Widerständen

#### Serienschaltung: "Summe der Widerstände"

Wenn man zwei Widerstände `R1, R2` in Serie (Reihe) schaltet, so werden sie
beide vom _gleichen Strom_ `I` durchflossen. Somit muss über dem ersten die
Spannung `U1=R1*I` stehen (oder sogenannt "abfallen"), über dem zweiten
die Spannung `U2=R2*I`. Die gesamte Spannung über beiden Widerständen
beträgt dann `U=U1+U2`, und da der Widerstand der Gesamtschaltung durch
`U=Rs*I` definiert ist, ergibt sich

    Rs*I=U=U1+U2=R1*I+R2*I=(R1+R2)*I
    Rs=R1+R2

![Serienschaltung]( img/widerstandserie.jpg )

Der Widerstandswert der Serienschaltung beträgt also
soviel wie die Summe der Teilwiderstände.

#### Parallelschaltung: "Summe der Leitwerte"

Schaltet man die beiden Widerstände jedoch parallel, so muss über beiden
_dieselbe Spannung_ `U` abfallen. Somit muss durch den ersten der Strom
`I1=U/R1` fliessen, durch den zweiten `I2=U/R2`, insgesamt also `I=I1+I2`.
Der Gesamtwiderstand `Rp` der Parallelschaltung ist definiert durch
`U=Rp*I=Rp*(I1+I2)` oder

    U/Rp=I1+I2=U/R1+U/R2=U*(1/R1+1/R2)
    1/Rp=1/R1+1/R2

![Parallelschaltung]( img/widerstandparallel.jpg )

Somit beträgt der Leitwert der Serienschaltung soviel
wie die Summe der Teilleitwerte, oder der Kehrwert des Gesamtwiderstandes
beträgt soviel wie die Summe der Kehrwerte der Teilwiderstände.

## Elektrische Quellen

Elektrische Quellen werden meistens als Spannungs- oder Stromquellen
bezeichnet, aber die Unterscheidung ist etwas willkürlich: sie beruht
hauptsächlich auf ihrem sogenannten Innenwiderstand. Ihre Grundeigenschaft
ist die Fähigkeit, Ladungen an einem Anschluss abgeben und an einem
anderen wieder aufnehmen zu können.

### Spannungsquellen

Eine _ideale_ Spannungsquelle weist unabhängig vom Strom, den sie abgibt,
an ihren Anschlüssen eine konstante Spannung `U0` auf; ihr Strom kann deshalb
jeden beliebigen Wert annehmen.
Eine _reale_ Spannungsquelle kann im einfachsten Fall dargestellt werden
durch eine ideale Spannungsquelle, zu der ein _Widerstand in Serie_
geschaltet ist, ihr sogenannter Innenwiderstand `Ri`.

Wenn die Quelle keinen Strom erzeugt, liegt an den Anschlüssen (Klemmen) die
Spannung der idealen Quelle an, die sogenannte Leerlaufspannung `U0`.
Mit zunehmendem Strom ("Belastung") fällt am Innenwiderstand eine
zunehmende Spannung ab, die somit an den äusseren Anschlüssen der
realen Quelle nicht mehr zur Verfügung steht.  Wenn die Spannungsquelle
kurzgeschlossen wird, so wird der maximale Strom `Imax` nur durch die
Leerlaufspannung sowie den Innenwiderstand bestimmt:

    Imax = U0 / Ri

Eine gute Spannungsquelle zeigt wenig Schwankungen der Klemmenspannung,
wenn sie belastet, d.h wenn Strom gezogen wird: je _kleiner_ ihr
Innenwiderstand ist, desto mehr Strom kann sie liefern, und desto _besser_
ist sie.

### Stromquellen

Eine _ideale_ Stromquelle gibt unabhängig von der Spannung, die an ihren
Anschlüssen vorliegt, einen konstanten Strom `I0` ab; ihre Spannung kann deshalb
jeden beliebigen Wert annehmen.
Eine _reale_ Stromquelle kann im einfachsten Fall dargestellt werden
durch eine ideale Stromquelle, zu der ein _Widerstand parallel_
geschaltet ist, ihr Innenwiderstand `Ri`.

Wenn die Quelle kurzgeschlossen ist, fliesst der Kurzschlussstrom `I0`,
der durch die ideale Stromquelle gegeben ist.  Mit zunehmender Spannung
("Belastung") fliesst durch den Innenwiderstand ein zunehmender Strom,
der somit an den äusseren Anschlüssen der realen Quelle nicht mehr
zur Verfügung steht.  Wenn die Stromquelle nicht angeschlossen ist,
so wird ihre maximale Spannung `Umax` durch den Kurzschlussstrom sowie
den Innenwiderstand bestimmt:

    Umax = I0 * Ri

Eine gute Stromquelle zeigt wenig Schwankungen des Klemmenstromes,
wenn sie belastet wird, d.h wenn Spannung vorliegt: je _grösser_ ihr
Innenwiderstand ist, desto mehr Spannung kann sie liefern, und desto
_besser_ ist sie.

### Beispiele gängiger Gleichstrom/spannungs-Quellen

#### Batterien

Als Batterien im engeren Sinne werden sogenannte Primärzellen bezeichnet,
die also aus der chemischen Energie ihrer Bestandteile elektrische erzeugen.
Sie sind nicht wieder aufladbar (mit wenigen seltenen Ausnahmen) und können
deshalb nur einmal verwendet werden.

Ihre wichtigsten Eigenschaften sind die Bauform und die Leerlaufspannung.
Im allgemeinen haben sie umso grössere Kapazität und kleineren Innenwiderstand,
je grösser ihr Volumen ist.

#### Akkumulatoren

Akkumulatoren werden auch als Sekundärzellen bezeichnet: sie müssen durch
eine andere Quelle aufgeladen werden, können dafür jedoch mehrfach wieder
verwendet werden. Im übrigen haben sie ähnliche Eigenschaften wie Batterien.

Das Aufladen darf nur durch geeignete Schaltungen erfolgen, da bei zu hohen
Ladeströmen oder anderen Fehlbehandlungen Brand- und Explosionsgefahr besteht,
insbesondere bei Lithium-Ionen-Akkus und ähnlichen neueren Typen.

#### Netzgeräte

Für stationären oder mobilen Betrieb eignen sich Netzgeräte, da infolge
niedriger Wirkungsgrade die Kosten bei Versorgung durch Batterien oder
Akkus erheblich teuer sind.

Einfache Netzgeräte sind Spannungsquellen; bessere Labornetzgeräte können
sowohl als Spannungs- als auch als Stromquelle verwendet werden.

##### Einstellung von Maximalstrom und -spannung

Wenn ein Labornetzgerät sowohl Strom als auch Spannung limitieren kann,
sollte man die Werte wie folgt einstellen:

1. Spannung auf Minimum setzen aber grösser als 0, sodass Regelung funktioniert
2. Strom auf Minimum setzen, Stromanzeige wählen (oder Ampèremeter einfügen)
3. Ausgang kurzschliessen, sodass Stromregelung anspricht
4. gewünschten Maximalstrom einstellen (falls nicht möglich, Spannung leicht
   erhöhen)
5. Kurzschluss entfernen
6. gewünschte Maximalspannung einstellen

## Multimeter

Das wichtigste Instrument beim Umgang mit elektrischen Schaltungen ist
ein sogenanntes Multimeter. Damit lassen sich im Minimum Spannungen,
Ströme und Widerstände bestimmen, bei teureren Geräten auch Sperrspannungen
von Dioden, Frequenzen von Wechselströmen, Kapazitätswerte von Kondensatoren
und Verstärkungsfaktoren von Transistoren oder andere Eigenschaften diskreter
elektrischer und elektronischer Bauteile.

Es gibt analoge und digitale Multimeter. Erstere bedingen etwas mehr
Verständnis zur korrekten Benutzung und sind oft teurer sowie empfindlicher auf Fehlbedienung,
arbeiten aber unter schwierigen Umständen meist zuverlässiger. Falls eine
sehr hohe Genauigkeit nötig ist oder extrem kleine Spannungen oder Ströme
gemessen werden müssen, sind digitale Multimeter von Vorteil.

### Grundlagen zum Verständnis analoger Multimeter

#### Zeigerinstrument zur Strommessung, Ampèremeter

Ströme oder Spannungen können gemessen werden, indem ihre chemischen
oder physikalischen Wirkungen bestimmt werden. Ein klassisches
Verfahren besteht darin, das Magnetfeld auszunutzen, das sich um einen
stromdurchflossenen Leiter bildet. Wir werden im weiteren nicht auf
magnetische Wirkungen eingehen und deshalb einfach als gegeben annehmen,
dass man durch geschickte Anordnung einer drehbaren Spule in einem
Magnetfeld aufgrund der Spulendrehung die Stärke des sie durchfliessenden
Stromes messen kann. Damit kann man ein sogenanntes Zeigerinstrument
bauen, welches das Kernstück eines analogen Multimeters darstellt.

Das Zeigerinstrument weist einen Innenwiderstand `Ri` auf:
wenn es von einem Strom `Iz` durchflossen wird, der Vollausschlag bewirkt,
besteht zwischen seinen beiden Anschlüssen eine Spannung `Uz=Ri*Iz`.
Im Idealfall wäre dieser Innenwiderstand gleich Null, da dann das
Instrument keine Leistung `Pz=Uz*Iz` aufnähme. Reale Geräte benötigen zu
ihrer Funktion selbstverständlich eine gewisse Leistung `Pz>0`, welche die
Messung leicht verfälscht.
Die Qualität eines Messgerätes zeigt sich
unter anderem darin, wie wenig es die auszumessende Schaltung belastet,
und sie ist umso höher, je kleiner `Ri` ist.

Ein solches Zeigerinstrument kann die Stärke des durchfliessenden Stromes
messen und stellt somit ein sogenanntes Ampèremeter dar. Es kann jedoch nur
Ströme messen, die kleiner als der Maximalstrom `Iz` sind. Damit man auch
stärkere Ströme messen kann, bedarf es einer

#### Messbereichserweiterung

Hierbei wird ein Teil des zu messenden Stromes am Zeigerinstrument vorbei
über einen oder mehrere (möglichst konstante) parallel geschaltete
Widerstände geleitet, sodass der über das Instrument fliessende Teil
nicht grösser als `Iz` wird.

Wenn bei Vollausschlag der totale zu messende Strom `Im` sein soll, so
muss `Ip=Im-Iz` über den Parallelwiderstand fliessen. Die Spannung,
die sowohl über dem Zeigerinstrument als auch über dem Widerstand
stehen muss, ist gegeben durch `Uz=Ri*Iz`. Dadurch ergibt sich für den
benötigten Parallelwiderstand der Wert `Rp=Uz/Ip=Uz/(Im-Iz)`.

#### Spannungsmessung, Voltmeter

Um eine Spannung mit dem Zeigerinstrument messen zu können, muss ein
(wiederum möglichst konstanter) Widerstand `Rs` in Serie zum Instrument
geschaltet werden. Dann ergibt sich nämlich bei Vollausschlag, dh einem
durch das Instrument und den Widerstand fliessenden Strom `Iz`, die
Gesamtspannung `U=Uz+Us=Ri*Iz+Rs*Iz=(Ri+Rs)*Iz`, oder für den benötigten
Serienwiderstand `Rs=(U-Ri*Iz)/Iz=U/Iz-Ri`. (Im Falle eines idealen
Instrumentes mit `Ri=0` wäre der Serienwiderstand einfach `Rs=U/Iz`.)

Ein Spannungsmessgerät oder Voltmeter ist umso besser, je grösser sein gesamter
Innenwiderstand `Ri+Rs=U/Iz` ist, dh je weniger Strom `Iz` sein
Zeigerinstrument für Vollausschlag benötigt.

#### Widerstandsmessung, Ohmmeter

Zur Widerstandsmessung muss das Gerät eine interne Batterie aufweisen. Damit wird an den
Messkontakten des Multimeters eine Spannung angelegt, und das Gerät zeigt
den dadurch erzeugten Strom an, der über das Ohm'sche Gesetz einen
Rückschluss auf den anliegenden Widerstand zulässt. Bei Multimetern
sind dazu spezielle direkt in Ohm bezeichnete Skalen vorhanden. Da die
Spannung der Batterie über längere Zeit abnimmt, kann die Nullstellung
(Vollausschlag) durch einen Trimmer eingestellt werden.

Eine solche Anordnung von Batterie und Ampèremeter wird auch als Ohmmeter
bezeichnet.

### Hinweise zur Benutzung von Multimetern

Digitale Multimeter sind meistens gut gegen Fehlmanipulationen geschützt.
Analoge Multimeter können durch falsche Bedienung jedoch leicht beschädigt
werden oder unsinnige Werte anzeigen.

Deshalb sind folgende Grundregeln zu beachten
(schaden auch nicht bei digitalen Geräten):

- Falls an Schaltungen mit gefährlichen Spannungen (über etwa 50V) gemessen
  wird, auf gute Isolation der Messsonden achten und mit nur einer Hand
  messen ("eine Hand im Hosensack"); dies vermeidet gefährliche Ströme über
  das Herz durch versehentliches Berühren spannungsführender Teile.
- Die richtigen Messkontakte wählen: im allgemeinen gibt es einen Anschluss
  für Masse (oft schwarz bezeichnet), einen für Spannungs- und
  Widerstandsmessung sowie einen weiteren für Strommessung; gelegentlich sind
  für den grössten Strommmessbereich ein oder zwei weitere spezielle Kontakte
  vorhanden.
- Messbereiche nur umschalten, wenn das Gerät nicht angeschlossen ist.
- Beim Messen mit dem grössten verfügbaren Bereich beginnen und solange
  zu kleineren Bereichen schalten, bis die beste Auflösung erreicht ist;
  ansonsten kann das Zeigerinstrument durch übermässigen Ausschlag
  überlastet werden (bei digitalen Geräten auf automatische
  Bereichswahl stellen).
- Wenn unklar ist, ob Wechsel- oder Gleichspannung vorliegt, mit einem
  Bereich für Wechselspannung beginnen und beide Polaritäten (Richtungen)
  messen; falls keine Wechselspannung messbar
  ist, wiederum mit dem grössten Bereich für Gleichspannung beginnen.
- Wenn Wechselspannung angezeigt wird, Kontakte umpolen und erneut messen;
  bei unterschiedlichen Werten dürfte (zudem oder nur) Gleichspannung
  vorliegen.
- Beim Messen von Gleichspannung oder Gleichstrom auf richtige Polung achten;
  falls unbekannt, mit grösstem Bereich kurz messen und bei falschem Ausschlag
  Gerät umpolen.
- Bevor Spannung gemessen wird, sicherstellen dass ein Spannungsbereich
  eingestellt ist und die Messkontakte für Spannung verwendet werden,
  da ansonsten ein Kurzschluss erfolgen würde, der das Gerät oder
  zumindest eine eingebaute Sicherung beschädigen kann.
- Widerstände nie unter Spannung messen, da das Gerät durch den Fremdstrom
  beschädigt werden kann; Vorsicht bei Kondensatoren (Kapazitäten) in der
  Schaltung, die auch nach dem Ausschalten Restspannungen aufweisen können!
- Vor jeder Widerstandsmessung (oder zumindest nach jedem Umschalten des
  Messbereiches) Messkontakte des Gerätes kurzschliessen und das Instrument
  mit dem Trimmer auf Vollausschlag (entspricht `0` Ohm) justieren; ist dies
  nicht möglich, ist die interne Batterie erschöpft oder das Gerät beschädigt.
- Widerstände in beide Richtungen (dh auch mit umgepolten Kontakten) messen:
  falls sich die Werte unterscheiden, kann es sich nicht um einen konstanten
  Widerstand handeln, sondern es könnten auch noch Dioden- oder andere
  Eigenschaften vorliegen.
- Falls sich der Widerstandswert während der Messung merklich ändert, dürfte
  Kapazität (in Form von diskreten oder parasitären Kondensatoren) vorhanden
  sein, oder der Widerstand wird durch einen externen Parameter (Temperatur,
  Magnetfeld, Licht etc) beeinflusst.


## nicht-ohmsche Bauteile

### Kondensator (Kapazität)

Ein Kondensator ist eine Anordnung von Leitern, die elektrische Ladung
speichern kann, aber im Gegensatz zu Batterie oder Akkumulator erfolgt
dies physikalisch und nicht chemisch. Im einfachsten Fall besteht ein
Kondensator aus zwei gegeneinander isolierten Leitern.

Die wichtigste Eigenschaft eines Kondensators ist seine Kapazität `K`
(üblicherweise mit `C` bezeichnet, aber dies kann leider leicht mit
dem Symbol für Coulomb verwechselt werden, der Einheit der Ladung).

Sie ist definiert durch die Ladungsmenge `Q`, die bei einer bestimmten
Spannung `U` auf dem Kondensator gespeichert werden kann: `Q=K*U`. Die
Einheit ist das Farad (F); 1 Farad = 1 Coulomb / 1 Volt ist eine sehr
grosse Kapazität, und Werte üblicher diskreter Kondensatoren liegen
im Bereich von Picofarad (`1e-12` F) bis Millifarad.

#### Plattenkondensator

Diskrete Kondensatoren sind meistens so aufgebaut, dass zwei leitende
Platten oder Folien gleicher Fläche `A` durch einen kleinen Luftspalt
oder eine isolierende Folie der Dicke `d` getrennt sind.  In diesem Fall
hängt die Kapazität proportional von der Fläche der gegenüberstehenden
Platten sowie umgekehrt proportional von ihrem Abstand ab:

     K=D*A/d

wobei `D` eine vom Material zwischen den Platten (dem sogenannten
Dielektrikum) abhängige Konstante ist, die für Luft etwa `8.85` pF/m beträgt.

### Elektromagnet

Bewegte elektrische Ladungen erzeugen ein Magnetfeld, und Magnetfelder
können bewegte Ladungen ablenken; dies bildet die Grundlage für
elektromagnetische Lautsprecher, Schalter und Transformatoren.
Sie enthalten Spulen aus aufgewickelten Leitern, um den Magneteffekt
zu verstärken. Gleichstrommässig gesehen ist eine Spule ein kleiner
Widerstand oder gar Kurzschluss. Mit einem Ohmmeter kann man
feststellen, ob die Spule unterbrochen ist oder unerwünschten Kontakt
zu anderen Leitern hat, aber im allgemeinen nicht, ob die Spule innere
Kurzschlüsse aufweist, da ihr Widerstand schon im Normalfall gering ist.

_Transformatoren_ funktionieren nur mit Wechselstrom, wobei eine Spule
ein magnetisches Wechselfeld erzeugt, welches in einer zweiten Spule
eine elektrische Spannung induziert.
Wir gehen im folgenden nicht weiter darauf ein.

_Lautsprecher_ bewegen eine Membran durch eine in einem Permanentmagnetfeld
angeordnete stromdurchflossene Spule; umgekehrt kann ein solcher
Lautsprecher auch als Mikrophon dienen. Mit einem Ohmmeter kann man
Unterbruch feststellen, und zudem sollte man ein Knacken hören, wenn
es an den Lautsprecher angeschlossen oder von ihm getrennt wird.

Durch Kombination einer Spule mit einem magnetischen Schalter erhält man
ein sogenanntes _Relais,_ welches das Schalten eines Laststromkreises
durch einen Steuerstromkreis ermöglicht. Zusätzlich zur Prüfung
der Spule kann man mit einem Ohmmeter die Belegung der Schaltkontakte
feststellen: es gibt sogenannte "Normally Open" (NO) und "Normally Closed"
(NC) Kontakte; erstere sind offen, wenn der Steuerstrom fehlt, letztere
wenn er anliegt. Es kann auch ein Schalter mehrere Kontakte bedienen;
in diesem Falle ist der gemeinsame Kontakt (der zwischen den anderen
umgeschaltet wird) meistens als "Common" (CO) bezeichnet.

### Diode

Eine ideale Diode verhält sich wie ein ideales Rückschlagventil:
sie lässt in einer Richtung den Strom ungehindert passieren (stellt
also in der sogenannten Vorwärtsrichtung einen idealen Leiter dar),
blockiert ihn aber in der anderen Richtung vollständig (und bildet
somit einen Leitungsunterbruch in der sogenannten Sperrrichtung).

Reale Dioden weisen in Sperrrichtung einen sehr hohen Widerstand auf
(wobei ein sehr kleiner sogenannter Leckstrom fliesst),
können jedoch bei zu hoher Sperrspannung (der Durchbruchspannung)
durchschlagen und eventuell zerstört werden.  In Vorwärtsrichtung
bilden sie meistens einen gewissen Widerstand, bis die sogenannte
Durchlassspannung erreicht ist, anschliessend nimmt der Widerstand
merklich ab.
Insgesamt bildet sich somit eine charakteristische
Kennlinie aus, siehe folgendes idealisiertes Beispiel: hier beträgt
die Durchlassspannung 1 V und die Durchbruchspannung -10 V.

![Diodenkennlinie]( img/diodenkennlinie.png )

Mit einem Ohmmeter kann man die Polarität einer Diode feststellen,
sofern die interne Batterie eine Spannung aufweist, die mindestens
der Durchlassspannung der Diode entspricht. Dazu muss man jedoch die
Polarität der Messkontakte kennen; sie ist bei Analogmultimetern meistens
_umgekehrt_ als bei Spannungsmessung, dh der Anschluss für den positiven
Pol bei Spannungsmessung ist normalerweise der negative Pol bei Verwendung
als Ohmmeter. Am besten konsultiert man die Bedienungsanleitung des
Gerätes; falls nicht verfügbar, muss mit einem zweiten Multimeter die
Spannung an den Messanschlüssen beim Betrieb als Ohmmeter festgestellt
werden.

Da eine Diode in Vorwärtsrichtung bei Spannungen grösser als die
Durchlassspannung praktisch einem Kurzschluss gleichkommt, darf
sie nie direkt an eine Spannungsquelle angeschlossen werden, da
sie sonst wahrscheinlich zerstört wird: sie benötigt immer einen
Vorwiderstand (was dem Anschluss an eine Stromquelle gleichkommt),
damit der Strom begrenzt wird. Typische Kleinsignaldioden können
Ströme bis zu einigen Milliampère aufnehmen.

#### Leuchtdiode

Eine in Vorwärtsrichtung betriebene Diode kann Licht emittieren;
Leuchtdioden (englisch _Light Emitting Diodes,_ LEDs) sind dafür
speziell konstruiert. Typischerweise benötigen sie 5 mA für eine
durchschnittliche Leuchtstärke. Die Durchlassspannung hängt von
der Farbe der LED ab, da sie direkt mit der Energie der erzeugten
Lichtteilchen (Photonen) zusammenhängt: rote LEDs benötigen gegen
1.6 V, grüne LEDs etwa 1.8 V.
Keine LED für sichtbares Licht funktioniert nur mit einer 1.5 V Batterie!

In der folgenden Abbildung ist die Kennlinie einer Serienschaltung von
einem 1.8 kOhm Widerstand und einer grünen LED in
Durchlassrichtung dargestellt. Die Durchlass- oder Schwellspannung
beträgt etwa 1.8 V, die Steigung der Kennlinie oberhalb davon beträgt
546 Mikrosievert, entsprechend etwa 1.8 kOhm; dies ist ein sogenannter
differentieller Widerstand, da er durch die Steigung der I-V-Kennlinie
gegeben ist, nicht durch den absoluten Wert von Strom und Spannung.

![LED-Kennlinie]( img/ledgruen.jpg )

Wenn man parallel zu LED und Widerstand einen weiteren Widerstand von
3.3 kOhm schaltet, ergibt sich eine Kennlinie wie in der folgenden
Abbildung ersichtlich.

(Hier wie in der vorhergehenden Abbildung liegt der Koordinatenursprung in der linken unteren Ecke des Bildschirmgitters.)

![LED und Widerstand]( img/ledplus3300.jpg )

Die punktierte Hilfslinie zeigt eine Steigung von 301 Mikrosievert an,
was etwa 3.32 kOhm entspricht.

#### Photodiode

Analog zur LED kann eine Diode auch dazu verwendet werden, Licht
wahrzunehmen. Eine sogenannte Photodiode ist so konstruiert, dass sie
speziell empfindlich auf Photonen ist. Dazu muss sie in _Sperrrichtung_
betrieben werden, wobei der Leckstrom mit zunehmender Beleuchtung ansteigt.
Dies kann im allgemeinen bereits mit einem Ohmmeter nachgewiesen werden.

Da prinzipiell jede Diode diese Eigenschaft aufweist, können auch LEDs
und "normale" Dioden lichtempfindlich sein. Da diese Eigenschaft für
die Verwendung als gleichrichtendes Element unerwünscht ist, werden
"normale" Dioden meist lichtundurchlässig gekapselt. Man muss jedoch
an diesen Effekt denken, wenn man eine freiliegende Diode (zB auf einem
Wafer nach dem Prozessieren) ausmisst: gelegentlich muss man das Licht
für eine genaue Messung ausschalten oder die Probe abdecken.

## Prober

*Dieser Teil ist noch in Arbeit und unvollständig!*

Ein Prober ist ein System von Strom- oder Spannungsquellen sowie
Multimeter(n), die durch eine Zentraleinheit gesteuert werden. Man kann
damit (Gleichspannungs/strom-) Kennlinien von diskreten Bauteilen oder
von Komponenten auf Halbleiterwafern aufnehmen.

Das Messprinzip ist wie folgt: Eine Strom- oder Spannungsquelle wird
an zwei Punkten der Schaltung oder des Bauelements angeschlossen, ein
Volt- oder Ampèremeter an zwei anderen (oder auch denselben) Punkten.
Dann wird der Bereich der Quelle gewählt sowie die Anzahl Messpunkte
oder Schrittweite der Quelle und die Messgeschwindigkeit. Die Steuerung
gibt dann die entsprechenden (x-) Werte über die Quelle an die Schaltung
aus, misst bei jedem Punkt den Strom oder die Spannung (y-Wert) über das
Multimeter und zeigt die entsprechenden (x,y)-Werte auf einem Bildschirm
an oder legt sie in einer Datei ab für spätere Analyse.

### Tektronix 370A

_in Arbeit; Checkliste für Grundeinstellungen_

#### Zweipol

- Config 3: COLLECTOR-OPEN-COMMON
- Leitungen: E Masse, C und B zusammen Versorgung und Messung
- Vertical Collector
- Horizontal Base/Emitter
- Polarity PlusPulse oder AC
- Acq Mode Norm
- Measurement Repeat
- Outputs Enabled
- Anschlussfeld Left oder Right
- Amplitude mit Variable Collector Supply
- Cursor
  - Schnittpunkt mit DOT
  - Steigung mit LINE

---

_Copyright 2014,2015, Yargo Bonetti_

Dieses Werk ist unter der Creative-Commons-Lizenz vom Typ Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International lizenziert.
Um eine Kopie dieser Lizenz einzusehen, besuchen Sie <http://creativecommons.org/licenses/by-sa/4.0/> oder schreiben Sie einen Brief an Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
